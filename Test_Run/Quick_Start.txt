#!/bin/bash
#PBS -P Quick_Start
#PBS -j oe
#PBS -N Quick_Start
#PBS -q parallel12
#PBS -l select=1:ncpus=12:mem=10gb
#PBS -l walltime=10:00:00

cd $PBS_O_WORKDIR;
np=$(cat ${PBS_NODEFILE} | wc -l);

source /etc/profile.d/rec_modules.sh

module load miniconda
bash
. ~/.bashrc

python Quick_Start.py