import ase.io
from basc.basc import BASC

# Read in your pre-relaxed surface slab and molecule
surface = ase.io.read("./samples/Fe2O3_1x1_relaxed.cif")
molecule = ase.io.read("./samples/CO.xyz")

# Make the BASC instance
basc = BASC(surface, molecule)

# Run BASC with all the default settings
point, energy, iter, solution = basc.run()

# Print and save the solution
print("Solution (%.6f eV) found on iteration %d" % (energy, iter))
ase.io.write("solution.cif", solution)
